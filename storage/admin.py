from django.contrib import admin
from storage.models import Page, PageContent, Video, Audio, Text


class PageContentAdmin(admin.TabularInline):
    model = PageContent
    extra = 1


class ContentAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']
    inlines = [PageContentAdmin]
    _state = None


class PageAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']
    inlines = [PageContentAdmin]


admin.site.register(Video, ContentAdmin)
admin.site.register(Audio, ContentAdmin)
admin.site.register(Text, ContentAdmin)
admin.site.register(Page, PageAdmin)

