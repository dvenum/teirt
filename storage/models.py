from django.db import models
from django.db import transaction
from django.core.validators import URLValidator


from teirt_api import consts

"""
1. Дана модель предметной области с сущностями:
    Страница (Page)

    Контент типа видео (Video)
    специфичные атрибуты: ссылка на видеофайл, ссылка на файл субтитров

    Контент типа аудио(Audio)
    специфичные атрибуты: битрейт в количестве бит в секунду

    Контент типа текст (Text)
    специфичные атрибуты: поле для хранение текста произвольной длинны

2. Нужно учитывать, что специфичные атрибуты разных видов контента существенно различаются.
3. У всех видов контента присутствует атрибут “счетчик просмотров” (counter).
4. У всех видов контента и страниц есть атрибут “заголовок” (title).
5. Со страницей может быть связан любой вид контента в любом количестве. Семантика такая: “на
страницу можно выложить любой вид контента в любом количестве”. Например: на странице может
быть 5 видео, 3 аудио и 7 текстов в любом порядке и в перемешку.
Следует учитывать, что в будущем виды контента могу добавляться и функционал должен легко
расширяться.

"""

class Page(models.Model):
    title = models.CharField(max_length=consts.MAX_TITLE_LENGTH)

    def __str__(self):
        return "{}: {}".format(self.id, self.title)


class PageContent(models.Model):
    page = models.ForeignKey('Page', on_delete=models.CASCADE)

    content_video = models.ForeignKey('Video', null=True, blank=True, on_delete=models.CASCADE)
    content_audio = models.ForeignKey('Audio', null=True, blank=True, on_delete=models.CASCADE)
    content_text = models.ForeignKey('Text', null=True, blank=True, on_delete=models.CASCADE)

    """ Desc: Не совсем ясно, что означает "в любом порядке и в перемешку",
        т.е. почему нельзя или только вперемешку или только в любом порядке.
        Будем считать, что требуется вручную задавать определенный порядок.
    """
    # A priority. Objects with a lower value are displayed first
    position = models.IntegerField(default=0)

    """ Desc: Это сделано через Null fields потому, что так быстрее.
        GenericFields делают бардак в таблицах и генерируют лишние запросы.
        Еще можно сделать через промежуточную таблицу, но здесь хватит и этого.

        Эту кучу if'ов можно как-то красивее оформить, но поскольку они быстрые
        и делают то, что нужно, меня их вид не очень смущает. 

        Когда количество видов контента начнет расти, модель можно вынести в отдельный
        модуль, вплоть до пары десятков это может быть терпимым.
    """
    @classmethod
    def increment_counters(cls, page_id):
        with transaction.atomic():
            """ Desc: Здесь можно сделать одним запросом, если класс Content
                не будет абстрактным.

                Если же counter хранится в разных таблицах, то это самое быстрое
                решение, которое мне удалось найти. Оно не требует пересылки данных
                в память python процесса и требует по одному запросу на тип контента.

                content_video__isnull=False можно не указывать, тк id в Video это pk

                Реализация через F позволяет избежать гонок.
            """
            s = models.Subquery(PageContent.objects.filter(page=page_id).values('content_video'))
            Video.objects.filter(id__in=s).update(counter=models.F('counter')+1)

            s = models.Subquery(PageContent.objects.filter(page=page_id).values('content_audio'))
            Audio.objects.filter(id__in=s).update(counter=models.F('counter')+1)

            s = models.Subquery(PageContent.objects.filter(page=page_id).values('content_text'))
            Text.objects.filter(id__in=s).update(counter=models.F('counter')+1)

    @property
    def content(self):
        if self.content_video is not None:
            return self.content_video
        if self.content_audio is not None:
            return self.content_audio
        if self.content_text is not None:
            return self.content_text

    @property
    def content_type(self):
        return (self.content_video or self.content_audio or \
                self.content_text).content_type

    @property
    def title(self):
        return (self.content_video or self.content_audio or \
                self.content_text).title
        

    @property
    def counter(self):
        return (self.content_video or self.content_audio or \
                self.content_text).counter


class Content(models.Model):

    # IntegerField() max value is 2147483647 only
    # BigIntegerField() is 9223372036854775807 max
    counter = models.BigIntegerField(default=0)
    title = models.CharField(max_length=consts.MAX_TITLE_LENGTH)
    content_type = 'Unknown'
    
    def __str__(self):
        return "{}: {}".format(self.id, self.title)

    class Meta:
        abstract = True


class Video(Content):
    content_type = "video"
    link = models.CharField(max_length=consts.MAX_URL_LENGTH)

    # Desc: ссылки нужно проверять, даже если они от своего сервера
    #       но стандартного URLValidator, я считаю, недостаточно.
    #       Если ссылки от пользователя, нужно отсечь попытки манипуляций, 
    #       а для своих, нужно проверять на соответствие формату.
    #   Доступность же файла пусть проверяет тот сервис, который его отдает.
    subtitles_link = models.CharField(max_length=consts.MAX_URL_LENGTH, validators=[URLValidator])
    width = models.IntegerField()
    height = models.IntegerField()

    # Desc: формат контейнера здесь не нужен (MKV, AVI), тк. 
    #       аудио и субтитры отдельно.
    encoder = models.CharField(max_length=12)  # h.264, h.265 and so on

    """ Desc: Сейчас редко есть возможность хранить каждое видео в одном определенном
        формате. Обычно нужны чуть ли не все и самых разных размеров.
        В таком случае, лучше сделать отдельный класс для VideoFile, а Video
        оставить как описатель самого ролика.
        Для проекта, больше всего напоминающего сервер рекламы, необходимо будет
        считать просмотры и по отдельным форматам видео. Я бы рекомендовал вынести
        все счетчики в отдельную таблицу, а каждому видео добавить uuid при
        регистрации его на сервере контента. И еще лучше сразу начать сохранять,
        кто его смотрел и почему. Когда станет нужно, данные уже будут, а это таки
        ценная информация.
    """


class Audio(Content):
    content_type = "audio"
    
    # Desc: В ТЗ не указано, что у аудио тоже есть ссылка и возможны варианты, 
    #       но все же отдавать аудио файлы отдельно от api самое разумное, 
    #       а уточнять для тестового задания не обязательно.
    #       Может быть и так, что аудио генерируется на лету, тогда здесь 
    #       все равно будет ссылка на источник (камеру/микрофон).
    link = models.CharField(max_length=consts.MAX_URL_LENGTH, validators=[URLValidator])

    fmt = models.CharField(max_length=6)    # AAC, PCM and so on
    bitrate = models.IntegerField()         # kb per second


class Text(Content):
    content_type = "text"

    # Desc: Текст также нужно чистить от подозрительных вещей.
    #  Пока не ясно, как его будут отдавать и что он такое, можно этого не делать.
    s = models.TextField()

