teirt
=================

just-work test assignment
https://drive.google.com/file/d/1eXr3-aa2KU-lpK7vtELSugD64EN0kH-w/view


Installation
=================

		git clone https://gitlab.com/dvenum/teirt.git
		cd teirt
		docker-compose build


Run
=================

apply migrations:

		docker-compose up migration

start web:

		docker-compose up runserver

to make tests

		docker-compose up autotests

web server is attaching to 0.0.0.0:8000


Visit http://localhost:8000/admin and use admin:aeb3EeGhaeki for login.
		* Press +Add for Pages.
		* Change content from list, only one from three
		* Add more contents as you like
		* Set position value for each
		* Press Save, repeat if you need

Visit http://localhost:8000/api/pages to see pages
Visist http://localhost:8000/api/video/1/ for details about content.
Press F5 to see counter raising.


Default credentials
=================

django admin:
		http://localhost:8000/admin
		username admin
		password maen7Aec

postgresql:

		http://localhost:8081
		username admin 
		password aeb3EeGhaeki


API samples
=================
		curl http://localhost:8000/api/pages/
		curl http://localhost:8000/api/pages/1/
		curl http://localhost:8000/api/text/1/


