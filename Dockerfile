#
#

FROM python:3.7-alpine
WORKDIR /code

ENV PYTHONUNBUFFERED 1

COPY requirements.txt requirements.txt
RUN \
    apk add bash postgresql-client && \
    apk add postgresql-libs postgresql-dev && \
    apk add --virtual .build-deps gcc musl-dev postgresql-dev && \
    pip install -r requirements.txt && \
    apk --purge del .build-deps

COPY . .

