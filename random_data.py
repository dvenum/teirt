#!/usr/bin/env python3

import math
import random
import uuid
from datetime import datetime

import pylunar
from faker import Faker


SERVER_NAME = "hotcontent.com"
OUTPUT_FILE = "random_data.csv"

# Moscow gps
La = (55, 45, 20.97)
Lo = (37, 37, 2.2728)

sizes = [
    (120, 90), (352, 240), (352, 288), (480, 272), (352, 480), (352, 576),
    (480, 480), (480, 576), (352, 480), (528, 480), (544, 480), (640, 480),
    (704, 480), (720, 480), (480, 576), (544, 576), (704, 576), (720, 576),
    (720, 480), (720, 576), (1280, 720), (1366, 768), (1440, 1080), (1920, 1080),
    (3840, 2160), (7680, 4320), (15360, 8640) 
]

encoders = [
        'h.264', 'h.265', 'AV1', 'h.261'
]

audio_formats = [
        'PCM', 'AAC',
]


def random_amount(phase):
    r0 = random.random() * phase
    while True:
        r0 = r0 * 10
        if r0 > 1000:
            break

    return int(r0)


def best_part(phase):
    """ return the best number from phase
    """
    return int(math.frexp(phase * 10**random.randint(0,8))[0] * 10)


def random_title(factory, phase):
    words = factory.words(best_part(phase))
    words[0] = words[0].capitalize()
    return ' '.join(words)


def random_link(phase, t):
    return "https://{}/{}/{}".format(SERVER_NAME, t, uuid.uuid1())


#video: title, link, subtitles_link, width, height, encoder
def random_video(factory, phase):
    return "video;{};{};{};{};{};{}".format(
                random_title(factory, phase),
                random_link(phase, 'video'),
                random_link(phase, 'subtitles'),
                *random.choice(sizes),
                random.choice(encoders)
            )


#audio: title, link, fmt, bitrate
def random_audio(factory, phase):
    return "audio;{};{};{};{}".format(
            random_title(factory, phase),
            random_link(phase, 'audio'),
            random.choice(audio_formats),
            1024*(best_part(phase**2))
        )


#text:  title, text
def random_text(factory, phase):
    return "text;{};{}".format(
            random_title(factory, phase),
            factory.text(max_nb_chars=8192).replace('\n','$$')
        )


CONSTRUCTORS = {
        0: random_video,
        1: random_audio,
        2: random_text
}

def generate(amount, phase):

    factory = Faker()

    with open(OUTPUT_FILE, 'w') as of:
        for _ in range(amount):
            t = random.randint(0,len(CONSTRUCTORS)-1)
            line = CONSTRUCTORS.get(t)(factory, phase)
            of.write(line + '\n')
            if len(line) > 90:
                line = line[:90] + " .."
            print(line)

    print("generated {} items".format(amount))


if __name__ == '__main__':
    mi = pylunar.MoonInfo(La, Lo)
    mi.update(datetime.utcnow())

    phase = mi.fractional_phase()
    amount = random_amount(phase)

    generate(amount, phase)


