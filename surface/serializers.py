"""


"""
from __future__ import absolute_import, unicode_literals
from rest_framework import serializers

from surface import tasks
import storage.models as models


class PageContentSerializer(serializers.ModelSerializer):

    details = serializers.SerializerMethodField()

    class Meta:
        model = models.PageContent
        fields = ('id','title', 'content_type', 'position', 'counter', 'details')

    def get_details(self, obj):
        return "/api/{}/{}".format(obj.content_type, obj.id)


class PagesSerializer(serializers.HyperlinkedModelSerializer):

    details = serializers.SerializerMethodField()

    class Meta:
        model = models.Page
        fields = ['id', 'title', 'details']

    def get_details(self, obj):
        return "/api/pages/{}".format(obj.id)

    def update_counters(self, instance):
        pass


class PageSerializer(serializers.HyperlinkedModelSerializer):

    content = serializers.SerializerMethodField()

    class Meta:
        model = models.Page
        fields = ['id', 'title', 'content']

    def update_counters(self, instance):
        tasks.inc_counters.apply_async( (instance.id,) )

    def get_content(self, obj):
        qset = models.PageContent.objects.filter(page=obj)
        return [ PageContentSerializer(m).data for m in qset ]


"""
    Desc: Классы одинаковые, но поскольку в реальности у них будут разные
          функции, пусть будет по классу на каждый вид контента
"""
class VideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Video
        fields = '__all__'


class AudioSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Audio
        fields = '__all__'


class TextSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Text
        fields = '__all__'

