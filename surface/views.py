"""

"""

from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import viewsets

import storage.models as models
from surface.serializers import (
        PageSerializer, PagesSerializer, 
        VideoSerializer, AudioSerializer, 
        TextSerializer
)


class PageViewSet(viewsets.ModelViewSet):

    queryset = models.Page.objects.all().order_by('-id')

    def get_serializer_class(self):
        path = self.request._request.path
        if self.action == 'create':
            return PagesSerializer

        return PageSerializer

    def list(self, request):

        queryset = models.Page.objects.all()
        serializer = PagesSerializer(queryset, many=True)
        return Response(serializer.data)


    def retrieve(self, request, pk=None):

        queryset = models.Page.objects.all()
        page = get_object_or_404(queryset, pk=pk)
        serializer = PageSerializer(page)
        serializer.update_counters(page)
        return Response(serializer.data)


class Content(object):
    """

    """

    @property
    def model(self):
        return self.serializer_class.Meta.model

    def retrieve(self, request, pk=None):

        queryset = self.model.objects.all()
        obj = get_object_or_404(queryset, pk=pk)
        serializer = self.serializer_class(obj)
        return Response(serializer.data)


class VideoViewSet(Content, viewsets.ModelViewSet):

    queryset = models.Video.objects.all().order_by('-id')
    serializer_class = VideoSerializer


class AudioViewSet(Content, viewsets.ModelViewSet):

    queryset = models.Audio.objects.all().order_by('-id')
    serializer_class = AudioSerializer


class TextViewSet(Content, viewsets.ModelViewSet):

    queryset = models.Text.objects.all().order_by('-id')
    serializer_class = TextSerializer
