"""


"""

from rest_framework import routers
from django.urls import path, include
from surface import views


router = routers.DefaultRouter()
router.register(r'pages', views.PageViewSet)
router.register(r'video', views.VideoViewSet)
router.register(r'audio', views.AudioViewSet)
router.register(r'text', views.TextViewSet)


urlpatterns = [
    path('', include(router.urls)),
]


