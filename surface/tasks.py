"""

"""

from celery import shared_task

from storage import models


@shared_task
def inc_counters(page_id):

    models.PageContent.increment_counters(page_id)
    
    return True


