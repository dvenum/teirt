from django.test import TestCase

import requests
import json
import time


def get_json(url):
    r = requests.get(url)
    return json.loads(r.text)


class RestMethodTest(TestCase):

    #def test_page_api(self):
        #data = get_json("http://runserver:8000/api/pages/1")
        #self.assertEqual(data['id'], 1)

    def test_video_api(self):
        data = get_json("http://runserver:8000/api/video/2")
        self.assertEqual(data['id'], 2)

    def test_audio_api(self):
        data = get_json("http://runserver:8000/api/audio/1")
        self.assertEqual(data['id'], 1)

    def test_text_api(self):
        data = get_json("http://runserver:8000/api/text/1")
        self.assertEqual(data['id'], 1)


    """ Desc: я не догадался сразу сделать генерацию и страниц тоже,
              так что тест не доделан из-за лимита времени
    """
    #def test_content_counter(self):
        #data = get_json("http://runserver:8000/api/page/1")
        #self.assertEqual(counterA+1, counterB)

