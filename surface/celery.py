from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from teirt_api import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'teirt_api.settings')

app = Celery('surface', broker=settings.CELERY_BROKER_URL)

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


